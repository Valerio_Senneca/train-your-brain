# Train Your Brain

## Comunicazione 1 <-> n

### Cosa

- i dati dei Pokemon vanno recuperati in modo asincrono dal file json annesso e visualizzati, con uno stile a piacere, nella homepage di progetto
- nella stessa pagina, nell'header, c'è una icona che indica il Pokedex
- ogni Pokemon visualizzato ha un bottone per aggiungerlo al Pokedex 
- tale bottone cambia aspetto e funzione (aggiunta/rimozione) se il prodotto è aggiunto al Pokedex o no
- la icona del Pokedex nell'header ha un counter che aggiorna il proprio numero ad ogni Pokemon aggiunto
- al clic sul Pokedex si vuole mostrare una tendina con i nomi dei Pokemon aggiunti e una X accanto a ciascuno per rimuoverlo

### Come
 
- scrivere codice al meglio  delle tue possibilità (scalabile, testabile, performante, comprensibile, ecc)
- non è un Code War, il codice deve essere leggibile e manutenibile
- puoi dividere in più file se lo desideri
- no framework js
- es6/7 ok
- jquery ok
- css non è importante ai fini del test
